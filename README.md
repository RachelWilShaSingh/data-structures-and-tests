# Data Structures and Tests

A set of already-written data structures and tests to go along with them. Meant for teachers who need automated tests as well as the structure; can write assignments using, and students can use the tests to check themselves and you for grading.